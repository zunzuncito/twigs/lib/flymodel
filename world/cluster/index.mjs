
import Terrain from './terrain.mjs'
import Env from './env.mjs'

export default class FlyWorldCluster {
  static generate(x, y, width, height, cfg) {
    const terrain = Terrain.generate(x, y, width, height, cfg);
    const env = Env.generate(terrain, x, y, width, height, cfg);
    return {
      terrain: terrain,
      env: null,
      x: x,
      y: y
    };
  }

  static generate_center_linear(x, y, width, height, cx, cy, cfg) {
    const terrain = Terrain.generate_center_linear(x, y, width, height, cx, cy, cfg);
    const env = Env.generate(terrain, x, y, width, height, cfg);
    return {
      terrain: terrain,
      env: null,
      x: x,
      y: y
    };
  }


  static generate_center_pow(x, y, width, height, cx, cy, cfg) {
    const terrain = Terrain.generate_center_pow(x, y, width, height, cx, cy, cfg);
    const env = Env.generate(terrain, x, y, width, height, cfg);
    return {
      terrain: terrain,
      env: null,
      x: x,
      y: y
    };
  }
}
