
import ImprovedNoise from "../../util/improved_noise.mjs"

export default class FlyTerrain {

  static max_quality(no_layers, quality_multiplier) {
    return Math.pow(quality_multiplier, no_layers-1);
  }

  static layers(no_layers, quality_multiplier, layer_cb) {
    let quality = 1;
    for (var j = 0; j < no_layers; j++) {
      layer_cb(quality);
      quality *= quality_multiplier;
    }
  }

  static indexes(width, height, cluster_x, cluster_y, index_cb) {
    const size = width * height;
    const world_x = (cluster_x * width - cluster_x) || 0;
    const world_y = (cluster_y * height - cluster_y) || 0;
    for (var i = 0; i < size; i++) {
      var x = i % width + world_x;
      var y = (~~(i / height)) / width * height + world_y;
      index_cb(i, x, y);
    }
  }

  static perlin_noise(x, y, z, quality, perlin) {
    return Math.abs(perlin.noise(x / quality, y / quality, z) * quality);
  }


  static normalize(heights, highest, smallest, bits) {
    let max = undefined;
    let uint_array = undefined;
    switch (bits) {
      case 8:
        max = 255;
        uint_array = new Uint8Array(heights.length);
        break;
      case 16:
        max = 65535;
        uint_array = new Uint16Array(heights.length);
        break;
      case 32:
        max = 4294967295
        uint_array = new Uint32Array(heights.length);
        break;
      default:
        throw new Error(`Invalid bit size for unsigned integer array: ${bits}`);
    }

    let height = highest - smallest;
    for (let i = 0; i < heights.length; i++) {
      uint_array[i] = Math.round((heights[i] - smallest) / height * max);
    }

    return uint_array;
  }


  static lod(heights, width, height, level) {
    const l_width = Math.ceil(width * level);
    const l_height = Math.ceil(height * level);

    const width_ratio = width / l_width;
    const height_ratio = height / l_height;

    let lod = new Array(l_width * l_height);

    for (let l_x = 0; l_x < l_width; l_x++) {
      for (let l_y = 0; l_y < l_height; l_y++) {
        const c_i = l_y * l_height + l_x;
        let x = Math.ceil(l_x * width / (l_width-1)) - 1;
        if (x < 0) x = 0;
        let y = Math.ceil(l_y * height / (l_height-1)) - 1;
        if (y < 0) y = 0;
        const i = Math.round(y * height + x);

        lod[c_i] = heights[i];
      }
    }

    return {
      heights: lod,
      width: l_width,
      height: l_height,
      ratio: level
    };
  }

  static lods(heights, width, height, cfg) {
    const lods = [{
      heights: heights,
      width: width,
      height: height
    }];

    if (cfg.lods) {
      for (let lod of cfg.lods) {
        const lod_obj = FlyTerrain.lod(lods[0].heights, width, height, lod);
        lods.push(lod_obj);
      }
    }

    return lods;
  }

  static generate_heights(cluster_x, cluster_y, width, height, cfg) {
    const heights = new Array(width * height).fill(0);
    const perlin = new ImprovedNoise();
    const z = cfg.seed;
    FlyTerrain.layers(cfg.layers, cfg.quality_multiplier, (quality) => {
      FlyTerrain.indexes(width, height, cluster_x, cluster_y, (i, x, y) => {
        heights[i] += FlyTerrain.perlin_noise(x, y, z, quality, perlin);
      });
    });
    return heights;
  }

  static generate(cluster_x, cluster_y, width, height, cfg) {
    const heights = FlyTerrain.generate_heights(cluster_x, cluster_y, width, height, cfg);
    return FlyTerrain.lods(heights, width, height, cfg);
  }

  static generate_center_linear(cluster_x, cluster_y, width, height, center_x, center_y, cfg) {
    const heights = FlyTerrain.generate_heights(cluster_x, cluster_y, width, height, cfg);

    FlyTerrain.indexes(width, height, cluster_x, cluster_y, (i, x, y) => {
      const dist_x = Math.abs(center_x - x);
      const dist_y = Math.abs(center_y - y);
//      const max_distance = Math.sqrt(center_x * center_x + center_y * center_y);
      const max_distance = (center_x+center_y) / 2;
      const distance = Math.sqrt(dist_x * dist_x + dist_y * dist_y);

      const increment = (max_distance - distance) / max_distance * cfg.center_multiplier;
      heights[i] += increment;
    });

    return FlyTerrain.lods(heights, width, height, cfg);
  }

  static generate_center_pow(cluster_x, cluster_y, width, height, center_x, center_y, cfg) {
    const heights = FlyTerrain.generate_heights(cluster_x, cluster_y, width, height, cfg);

    FlyTerrain.layers(cfg.layers, cfg.quality_multiplier, (quality) => {
      FlyTerrain.indexes(width, height, cluster_x, cluster_y, (i, x, y) => {
        const dist_x = Math.abs(center_x - x);
        const dist_y = Math.abs(center_y - y);
//        const max_distance = Math.sqrt(center_x * center_x + center_y * center_y); 
        const max_distance = (center_x+center_y) / 2;
        const distance = Math.sqrt(dist_x * dist_x + dist_y * dist_y);
        const increment = Math.pow((max_distance - distance) / max_distance, 2) * cfg.center_multiplier;
        heights[i] += increment;
      });
    });

    return FlyTerrain.lods(heights, width, height, cfg);
  }
}
