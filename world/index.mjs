
import Cluster from './cluster/index.mjs'
import Terrain from './cluster/terrain.mjs'

export default class FlyWorld {
  static ck_cfg(cfg) {
    if (typeof cfg.seed !== "number") cfg.seed = 1;
    if (typeof cfg.layers !== "number") cfg.layers = 4;
    if (typeof cfg.quality_multiplier !== "number") cfg.quality_multiplier = 5;
    if (typeof cfg.bit_precision !== "number") cfg.bit_precision = 8;
  }

  static normalize(clusters, cfg) {
    let highest = 0;
    let smallest = undefined;
    for (let cluster of clusters) {
      for (let lod of cluster.terrain) {
        for (let h of lod.heights) {
          if (typeof smallest == "undefined" || h < smallest) smallest = h;
          if (h > highest) highest = h;
        }
      }
    }

    for (let c = 0; c < clusters.length; c++) {
      for (let l = 0; l < clusters[c].terrain.length; l++) {
        clusters[c].terrain[l].heights = Terrain.normalize(clusters[c].terrain[l].heights, highest, smallest, cfg.bit_precision);
      }
    }

  }

  static generate(width, height, cluster_width, cluster_height, cfg) {
    FlyWorld.ck_cfg(cfg);
    const clusters = [];

    for (let y = 0; y < height; y++) {
      for (let x = 0; x < width; x++) {
        clusters.push(Cluster.generate(x, y, cluster_width, cluster_height, cfg));
      }
    }
    FlyWorld.normalize(clusters, cfg);
    return clusters;
  }

  static generate_center_linear(width, height, cluster_width, cluster_height, cfg) {
    FlyWorld.ck_cfg(cfg);

    const clusters = [];

    const center_x = width * cluster_width / 2;
    const center_y = height * cluster_height / 2;

    for (let y = 0; y < height; y++) {
      for (let x = 0; x < width; x++) {
        clusters.push(Cluster.generate_center_linear(x, y, cluster_width, cluster_height, center_x, center_y, cfg));
      }
    }
    FlyWorld.normalize(clusters, cfg);

    return clusters;
  }

  static generate_center_pow(width, height, cluster_width, cluster_height, cfg) {
    FlyWorld.ck_cfg(cfg);
    const clusters = [];

    const center_x = width * cluster_width / 2;
    const center_y = height * cluster_height / 2;

    for (let y = 0; y < height; y++) {
      for (let x = 0; x < width; x++) {
        clusters.push(Cluster.generate_center_pow(x, y, cluster_width, cluster_height, center_x, center_y, cfg));
      }
    }
    FlyWorld.normalize(clusters, cfg);
    return clusters;
  }

  static Cluster = Cluster
}
